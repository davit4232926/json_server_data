export type valueType = string | number | boolean
export type itemType = {[key: string]: valueType}
export type itemTypeString = 'date' | 'email' | 'boolean' | 'number' | 'long' | 'normal'
