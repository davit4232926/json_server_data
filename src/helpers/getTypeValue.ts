import { itemTypeString, valueType } from '../type.ts';

export default function getTypeValue(str: valueType): itemTypeString {
  if (typeof str === 'boolean') {
    return 'boolean';
  }

  if (typeof str === 'number') {
    return 'number';
  }

  const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (emailRegex.test(str)) {
    return 'email';
  }

  const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2} \-\d{2}:\d{2}$/;
  if (dateFormat.test(str)) {
    return 'date';
  }

  if (str.length > 40) {
    return 'long';
  }

  return 'normal';
}
