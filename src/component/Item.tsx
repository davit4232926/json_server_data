import {
  FormEventHandler, Fragment, MouseEventHandler, useState,
} from 'react';
import { itemType, valueType } from '../type.ts';
import getTypeValue from '../helpers/getTypeValue.ts';
import { apiUrl } from '../utils/url.ts';

function Item({ obj }: {obj: itemType}) {
  const array = Array.from(Object.entries(obj));

  const [disabled, setDisabled] = useState<boolean>(true);

  const handleEnableEdit: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    setDisabled(false);
  };
  const handleEnableSave: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    const form = e.currentTarget;
    const formData = new FormData(form);

    const formValues: itemType = {};

    formData.forEach((value, key) => {
      const inputElement = form.elements.namedItem(key) as HTMLInputElement;
      if (inputElement.type === 'number') {
        formValues[key] = Number(value);
      } else if (inputElement.type === undefined) {
        formValues[key] = value === 'true';
      } else if (inputElement.type === 'datetime-local') {
        formValues[key] = value as string + obj[key].toString().slice(16);
      } else {
        formValues[key] = value as valueType;
      }
    });

    fetch(`${apiUrl}/${formValues.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formValues),
    }).then(() => setDisabled(true));
  };

  return (
    <form onSubmit={handleEnableSave}>
      {
          array.map(([key, value]) => {
            if (key === 'id') {
              return (
                <input key={key} name={key} type="hidden" value={value as string} />
              );
            }

            const type = getTypeValue(value);

            if (type === 'boolean') {
              return (
                <Fragment key={key}>
                  <input disabled={disabled} type="radio" name={key} value="true" defaultChecked={!!value} />
                  true

                  <input disabled={disabled} type="radio" name={key} value="false" defaultChecked={!value} />
                  false
                </Fragment>
              );
            }

            if (type === 'number') {
              return (
                <input key={key} name={key} disabled={disabled} type="number" defaultValue={value as number} />
              );
            }

            if (type === 'email') {
              return (
                <input key={key} name={key} disabled={disabled} type="email" defaultValue={value as string} />
              );
            }

            if (type === 'date') {
              return (
                <input
                  key={key}
                  disabled={disabled}
                  type="datetime-local"
                  name={key}
                  defaultValue={(value as string).slice(0, 16)}
                />
              );
            }

            if (type === 'long') {
              return (
                <textarea key={key} name={key} disabled={disabled} defaultValue={value as string} />
              );
            }

            return (
              <input key={key} name={key} disabled={disabled} defaultValue={value as string} />
            );
          })
        }
      { disabled ? <button type="button" onClick={handleEnableEdit}>Edit</button>
        : <button type="submit">Save</button>}
    </form>
  );
}

export default Item;
