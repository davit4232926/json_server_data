import { useEffect, useState } from 'react';
import Item from './component/Item.tsx';
import { itemType } from './type.ts';
import { apiUrl } from './utils/url.ts';

function App() {
  const [data, setData] = useState<itemType[]>([]);

  useEffect(() => {
    fetch(apiUrl).then((res) => res.json()).then((res) => {
      setData(res);
    });
  }, []);

  return (
    <div>
      {data.map((obj) => (
        <Item obj={obj} key={`${obj.id as string}`} />
      ))}
    </div>
  );
}

export default App;
