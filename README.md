# Project Server and Client

This project includes a server to serve JSON data and a client to interact with the server.

## Getting Started

### Prerequisites

Ensure you have the following installed:

- Node.js
- npm (Node Package Manager)

## Installation

To install node_modules directory please use command:
```bash
npm install
```

## Running the Server

To start the server and serve JSON data have *db.json* file, use the following command:
```bash
npm run server
```

## Running the Client

To start client open another terminal and use command:
```bash
npm run dev